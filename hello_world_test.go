package main_test

import (
	"testing"

	hw "bitbucket.org/nathan-rightscale/go-demo"
)

func Test_hw_SayHello(t *testing.T) {
	tests := []struct {
		name string
		h    hw.Hw
		want string
	}{
		{"Says: Hello World", hw.Hw{}, "Hello World"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.h.SayHello(); got != tt.want {
				t.Errorf("hw.SayHello() = %v, want %v", got, tt.want)
			}
		})
	}
}
