package main

type Hw struct{}

func New() *Hw {
	return new(Hw)
}

func (h Hw) SayHello() string {
	return "Hello World"
}

func (h Hw) SayCustom(s string) string {
	return s
}
