package main

import (
	"fmt"
)

func main() {
	// Create a new hw instance
	h := New()
	// Print SayHello
	fmt.Printf("hw says: %v", h.SayHello())
}
